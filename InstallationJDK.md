# Installer le JDK de Java

Le JDK de java vous permet de développer avec l'API de Forge dans notre cas.

## Installation
### Installer le JDK :
Pour l'installer le JDK de Java, allez sur le [site d'oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Accepter la licence et téléchargez la version adaptée à votre système (Généralement Windows x64)

Une fois, le téléchargement fini, il vous suffit de lancer l'application en administrateur.
Lorsque l'installation à fini, allez sur votre disque dur C:/ puis dans le dossier "Programmes" puis dans le dossier "``Java``". \n
Un dossier ayant le nom de la version de java que vous venez d'installer devrait être présent (``jdk1.8.0_191`` par exemple).
Copier le chemin complet de ce dossier (``C:\Program Files\Java\jdk1.8.0_191`` avec l'exemple d'au-dessus).

### Configuration du JDK

Ouvrez le panneau de configuration puis aller sur la page Système et à gauche cliquez sur "Paramètres système avancés", une page va s'ouvrir, cliquez sur l'onglet portant le même nom puis sur "Variables d'environnement…" 
Créez une nouvelle variable dans Variables utilisateur, dans le nom mettez ``JAVA_HOME`` et dans la valeur, copier le chemin du dossier où le jdk est installé.
Ensuite, sélectionnez la variable ``PATH`` (ou ``Path``), cliquez sur modifier... et à la suite ajouter une ligne, et coller à nouveau le chemin du jdk en ajoutant ``\bin``. Par exemple :

```C:\Program Files\Java\jdk1.8.0_191\bin```

## Conclusion

Java est maintenant installé et configuré. La manipulation dans les variables d'environnement sera à refaire à chaque mise à jour de java (remplacez l'ancien chemin dans PATH, n'ajoutez pas à la suite lors des mises à jour, même chose pour JAVA_HOME, écrasez l'ancienne valeur).

## Chaine :
[M4TH1EU_ YouTube ](https://www.youtube.com/watch?v=htQtYfAl39A&t=2s)