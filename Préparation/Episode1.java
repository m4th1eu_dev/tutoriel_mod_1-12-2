//###################################################################################################################################
//Class : Items.java

package ch.m4th1eu.reset;

import ch.m4th1eu.reset.items.ItemTUTO;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


@Mod.EventBusSubscriber(value = Side.CLIENT, modid = Main.MODID)
public class Items {
    public static final Item ItemTUTO = new ItemTUTO();

    public static void setItemName(Item item, String name) {
        item.setRegistryName(Main.MODID, name).setTranslationKey(Main.MODID + "." + name);
    }


    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerItemsModels(ModelRegistryEvent event) {
        registerModel(ItemTUTO, 0);
        //registerModel(BLOCK_TUTORIAL_ITEM, 0);
    }

    @SideOnly(Side.CLIENT)
    public static void registerModel(Item item, int metadata) {
        ModelLoader.setCustomModelResourceLocation(item, metadata, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }


}



//###################################################################################################################################
//Class : ItemTUTO.java

package ch.m4th1eu.reset.items;

import ch.m4th1eu.reset.Items;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class ItemTUTO extends Item {

    public static final String NAME = "item_tuto";

    public ItemTUTO() {
        super();

        Items.setItemName(this, NAME);
        setCreativeTab(CreativeTabs.MISC);
        setMaxStackSize(4);

    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        if (!world.isRemote) {
            player.sendMessage(new TextComponentString("§bBravo " + player.getName() + ", tu as réussi !"));
            return new ActionResult(EnumActionResult.SUCCESS, new ItemStack(this));
        } else {
            return new ActionResult(EnumActionResult.FAIL, new ItemStack(this));
        }
    }

}



//###################################################################################################################################
//Class : Main.java

package ch.m4th1eu.reset;

import ch.m4th1eu.reset.proxy.CommonProxy;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = Main.MODID, name = "Reset Mod", version = "0.1", acceptedMinecraftVersions = "[1.12.2]")
public class Main {

    public static final String MODID = "reset";

    @Instance(Main.MODID)
    public static Main instance;

    @SidedProxy(clientSide = "ch.m4th1eu.reset.proxy.ClientProxy", serverSide = "ch.m4th1eu.reset.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event.getSuggestedConfigurationFile());
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init();
    }

    public Main(){
        MinecraftForge.EVENT_BUS.register(new RegisteringHandler());
    }
}

###################################################################################################################################

