//Main.java

    public Main() {
        MinecraftForge.EVENT_BUS.register(new RegisteringHandler());
    }


//////////////////////////////////////////////////////////
    
package ch.m4th1eu.modtuto.items;

import ch.m4th1eu.modtuto.Main;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Items {

    public static final Item ItemTUTO = new ItemTUTO();

    public static void setItemName(Item item, String name) {
        item.setRegistryName(Main.MODID, name).setTranslationKey(Main.MODID + "." + name);
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerItemsModels(ModelRegistryEvent event) {
        registerModel(ItemTUTO, 0);
    }

    @SideOnly(Side.CLIENT)
    public static void registerModel(Item item, int metadata) {
        ModelLoader.setCustomModelResourceLocation(item, metadata, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}



////////////////////////////////////////////////////////

package ch.m4th1eu.modtuto.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemTUTO extends Item {

    public static final String NAME = "item_tuto";

    public ItemTUTO() {
        super();
        Items.setItemName(this, NAME);
        setCreativeTab(CreativeTabs.MISC);

        setMaxStackSize(4);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        System.out.println("Bravo petit message dans la console !");
        return null;
    }
}



/////////////////////////////////////////////////////////

package ch.m4th1eu.modtuto.items;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RegisteringHandler {

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(Items.ItemTUTO);
    }
}
