# Install Java JDK

The Java JDK allows you to develop with the Forge API in our case.

## Installation
### Install the JDK :
To install the Java JDK, go to the [Oracle site](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Accept the license and download the version adapted to your system (Generally Windows x64)

Once the download is finished, you just have to launch the application as administrator.
When the installation finishes, go to your hard disk ``C:/`` then in the folder "``Programs``" then in the folder "``Java``".
A folder with the name of the version of java you just installed should be present (``jdk1.8.0_191`` for example).
Copy the full path of this folder (``C:\Program Files\Java\jdk1.8.0_191`` with the example from above).

### JDK configuration

Open the control panel then go to the System page and left click on "Advanced System Settings", a page will open, click on the tab with the same name and then on "Environment variables ..."
Create a new variable in User Variables, in the name put ``JAVA_HOME`` and in the value, copy the path of the folder where the jdk is installed.
Next, select the `` PATH`` (or ``Path``) variable, click edit... and then add a line, and paste the jdk path again by adding ``\bin``. For example :

``C:\Program Files\Java\jdk1.8.0_191\bin``

## Conclusion

Java is now installed and configured. The manipulation in the environment variables will be to repeat every update of java (replace the old path in PATH, do not add following during updates, same thing for ``JAVA_HOME``, overwrite the old value).