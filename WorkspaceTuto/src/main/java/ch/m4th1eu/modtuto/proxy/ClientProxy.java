package ch.m4th1eu.modtuto.proxy;

import net.minecraftforge.common.MinecraftForge;

import java.io.File;

public class ClientProxy extends CommonProxy {

    public void preInit(File configFile) {
        super.preInit(configFile);
        System.out.println("Pre-init client side");
    }

    public void init() {
        super.init();
        MinecraftForge.EVENT_BUS.register(this);
    }
}
