package ch.m4th1eu.modtuto.proxy;

import java.io.File;

public class CommonProxy {

    public void preInit(File configFile) {
        System.out.println("Pre-init server side");
    }

    public void init() {
    }
}
