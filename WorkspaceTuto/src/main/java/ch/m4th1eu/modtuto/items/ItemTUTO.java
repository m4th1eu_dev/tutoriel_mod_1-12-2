package ch.m4th1eu.modtuto.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemTUTO extends Item {

    public static final String NAME = "item_tuto";

    public ItemTUTO(){
        super();

        Items.setItemName(this, NAME);
        setCreativeTab(CreativeTabs.MISC);

        setMaxStackSize(4);
    }
}
