package ch.m4th1eu.modtuto;


import ch.m4th1eu.modtuto.proxy.CommonProxy;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Main.MODID, name = "Tutorial Mod", version = "0.1")
public class Main {

    public static final String MODID = "modtuto";

    @SidedProxy(clientSide = "ch.m4th1eu.modtuto.proxy.ClientProxy", serverSide = "ch.m4th1eu.modtuto.proxy.CommonProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init();
    }

    public Main(){
        MinecraftForge.EVENT_BUS.register(new RegisteringHandler());
    }
}
